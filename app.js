const express = require('express')
const app = express();
const portNo = 3000;

//Mosca
require('./mqtt.js')


app.get('/',(req,res,next)=> {
    res.send("All is well")
})


app.listen(portNo, ()=>{
    console.log("App listening on port " + portNo)
})
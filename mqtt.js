const mosca = require('mosca')

const ascoltatore = {
    //using ascoltatore
    type: 'mongo',
    url: 'mongodb://localhost:27017/mqtt',
    pubsubCollection: 'ascoltatori',
    mongo: {}
};

const moscaSettings = {
    port: 1883,
    backend: ascoltatore,
    persistence: {
        factory: mosca.persistence.Mongo,
        url: "mongodb://localhost:27017/mqtt"
    }
}

var server = new mosca.Server(moscaSettings)

server.on('clientConnected', function (client) {
    console.log('client connected', client.id);
});


server.on('published', (packet, client) => {
    var topic = packet.topic;
    switch (topic) {
        case 'checkin':
            //UID
            console.log('Published: ' + packet.topic + ' ' + packet.payload);
            //Save to DB
            break;
        case 'presence':
            console.log('Joined: ' + packet.payload);
            break;
        default:
            console.log('Payload: ' + packet.payload);
            break;
    }
})

server.on('ready', () => {
    console.log("Mqtt broker is up and running")
})